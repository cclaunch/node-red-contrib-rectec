var TuyAPI = require('tuyapi');

module.exports = function(RED) {
    function RecTecNode(config) {
        RED.nodes.createNode(this, config);
        let node = this;

        this.on('input', function(msg, send, done) {
            // fall back to node.send() for Node-RED 0.x
            send = send || function() { node.send.apply(node, arguments) }

            try {
                var device = new TuyAPI({
                    ip: config.deviceIp,
                    key: config.localKey,
                    version: parseFloat(config.version)
                });
            } catch (err) {
                if (done) {
                    // Node-RED 1.0 compatible
                    done(err);
                } else {
                    // Node-RED 0.x compatible
                    node.error(err);
                }
            }

            (async () => {
                try {
                    await device.find();
                    await device.connect();
                } catch (err) {
                    if (done) {
                        // Node-RED 1.0 compatible
                        done(err);
                    } else {
                        // Node-RED 0.x compatible
                        node.error(err);
                    }
                }

                let raw_status = await device.get({schema: true});
                var status = {};

                status['power'] = raw_status['dps']['1'];
                status['set_temperature'] = raw_status['dps']['102'];
                status['actual_temperature'] = raw_status['dps']['103'];
                status['min_feedrate'] = raw_status['dps']['104'];
                status['food_temp1'] = raw_status['dps']['105'];
                status['food_temp2'] = raw_status['dps']['106'];
                status['temp_calibration'] = raw_status['dps']['107'];
                status['er1_alarm'] = raw_status['dps']['109'];
                status['er2_alarm'] = raw_status['dps']['110'];
                status['er3_alarm'] = raw_status['dps']['111'];

                msg.payload = status;

                device.disconnect();

                send(msg);

                // exist check for backwards compatibility with Node-RED < 1.0
                if(done) {
                    done();
                }
            })();
        });
    }
    RED.nodes.registerType("rectec", RecTecNode, {
        settings: {
            deviceIp: {
                value: "",
                exportable: true
            },
            localKey: {
                value: "",
                exportable: true
            },
            version: {
                value: "",
                exportable: true
            }
        }
    });
}
